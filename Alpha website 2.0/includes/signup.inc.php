<?php

if(isset($_POST['signup-submit'])){

  require 'dbh.inc.php';


  $username=$_POST['uid'];
  $pass1=$_POST['pass1'];
  $pass2=$_POST['pass2'];

  if(empty($username) || empty($pass1) || empty($pass2)){
    header("Location: ../index.php?error=emptyfields&uid=".$username);
    exit();

  }

  else if(!preg_match("/^[a-zA-Z0-9]*$/",$username)){
    header("Location: ../index.php?error=invaliduid");
    exit();
  }
  else if($pass1 !== $pass2){
    header("Location: ../index.php?error=passwordcheck");
    exit();
  }
  else{

    $sql = "SELECT username FROM users WHERE username=?";
    $stmt=mysqli_stmt_init($conn);

    if(!mysqli_stmt_prepare($stmt,$sql)){
      header("Location: ../index.php?error=sqlerror");
      exit();
    }
    else{
      mysqli_stmt_bind_param($stmt,"s",$username);
      mysqli_stmt_execute($stmt);
      mysqli_stmt_store_result($stmt);
      $resultCheck=mysqli_stmt_num_rows($stmt);
      if ($resultCheck>0){
        header("Location: ../index.php?error=usertaken");
        exit();
      }
      else{
        $sql = "INSERT INTO users (username,pass) VALUES (?,?)";
        $stmt=mysqli_stmt_init($conn);
        if(!mysqli_stmt_prepare($stmt,$sql)){
          header("Location: ../index.php?error=sqlerror");
          exit();
        }
        else{
          /*hashing password*/
          $hashedPwd=password_hash($pass1,PASSWORD_DEFAULT);
          mysqli_stmt_bind_param($stmt,"ss",$username,$hashedPwd);
          mysqli_stmt_execute($stmt);
          header("Location: ../index.php?signup=success");
          exit();
        }
      }

    }


  }

  mysqli_stmt_close($stmt);
  mysqli_close($conn);


}
else{
  header("Location: ../index.php?error=unauthorized");
  exit();

}
