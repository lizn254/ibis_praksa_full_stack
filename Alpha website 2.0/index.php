<?php

  require "header.php";

 ?>

  <main>

        <div class="container">
          <div class="login-box">
            <div class="row">
              <div class="col-md-6 login-left">
                <h2>Login here</h2>
                <form action="includes/login.inc.php" method="post">
                  <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" class="form-control" placeholder="Username" required>
                  </div>
                  <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="psw" class="form-control" placeholder="Password">
                  </div>
                  <button type="submit" class="btn btn-primary" name="login-submit">Login</button>
                  
                </form>
              </div>


              <div class="col-md-6 login-right">
                <h2>Sign Up Here</h2>
                <form action="includes/signup.inc.php" method="post">
                  <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="uid" class="form-control" placeholder="Username">
                  </div>
                  <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="pass1" class="form-control" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label>Repeat Password</label>
                    <input type="password" name="pass2" class="form-control" placeholder="Repeat password">
                  </div>
                  <button type="submit" class="btn btn-primary" name="signup-submit">Sign Up</button>
                </form>
              </div>
            </div>
          </div>
      </div>

  </main>



<?php

  require "footer.php";
 ?>
