<?php

if(isset($_POST['login-submit'])) {

  require 'dbh.inc.php';

  $userId=$_POST['username'];
  $userPass=$_POST['psw'];

  if (empty($userId)||empty($userPass)){
    header("Location: ../index.php?error=emptyfields");
    exit();
  }
  else{
    $sql = "SELECT * FROM users WHERE username=?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt,$sql)){
      header("Location: ../index.php?error=sqlerror");
      exit();
    }
    else{

      mysqli_stmt_bind_param($stmt,"s",$userId);
      mysqli_stmt_execute($stmt);
      $result=mysqli_stmt_get_result($stmt);
      if($row = mysqli_fetch_assoc($result)){
        $pwCheck = password_verify($userPass,$row['pass']);/*checks hashed passwords*/
        if($pwCheck==false){
          header("Location: ../index.php?error=wrongpassword");
          exit();
        }
        else if($pwCheck==true){
          session_start();
          $_SESSION['id']=$row['id'];
          $_SESSION['userId']=$row['username'];
          header("Location: ../index.php?login=success");
          exit();
        }
        else{
          header("Location: ../index.php?error=unexpectederror");
          exit();
        }
      }
      else{
        header("Location: ../index.php?error=nouser");
        exit();
      }
    }


  }
  mysqli_stmt_close($stmt);
  mysqli_close($conn);
}
else{
  header("Location: ../index.php?error=unauthorized");
  exit();
}
