<?php
  session_start();
  if($_SESSION['welcome']==1) {
    echo '<!DOCTYPE html>
    <html lang="en" dir="ltr">
      <head>

        <meta charset="utf-8">
        <meta name="viewport" contetnt="width=device-width">
        <meta name="description" content="Affordable and professional software - Alpha"
        <meta name="keywords" content="web design,free,easy websites"
        <meta name="author" content="Marko Vekaric">
        <title>Alpha | Main Page</title>
        <link rel="stylesheet" href="./css/reset.css"/>
        <link rel="stylesheet" href="./css/style.css"/>

      </head>
      <body>

        <header>
          <div class="container">
            <div id="branding">
              <h1><a class="highlight" href="#">Alpha</a> by HTML5 UP</h1>
            </div>
            <nav>
              <ul>
                <li id="l1"><a href="#">Home</a></li>
                <li id="l2"><a href="#">Layouts</a></li>
                <li id="l3"><a href="#">Sign Up</a></li>
              </ul>
            </nav>
          </div>
        </header>

        <section id="showcase">
          <div class="container">
            <div id="banner">
              <h1>Alpha</h1>
              <p>Another fine responsive site template freebie by HTML5 UP</p>
              <div id="buttons">
              <button type="button" class="button_1"><font class="white1">Sign Up</font></button>
              <button type="button" class="button_2"><font class="white2">Learn More</font></button>
              </div>
            </div>
          </div>
        </section>

        <section id="mainPart">
          <div class="container">
            <div id="introduction">
              <div id="heading">
                <h2 id="mainheading">Introducing the ultimate mobile app for doing stuff with your phone</h2>
              </div>
              <div id="paragraph">
                <p>Blandit varius ut praesent nascetur eu penatibus nisi risus faucibus nunc ornare adipiscing nunc adipiscing. Condimentum turpis massa.</p>
              </div>
              <div id="firstImg">
                <img src="img/prva.jpg">
              </div>
            </div>
            <div id="ribbons">
              <div class="cells" id="first">
                <div id="firstribbon">
                  <img class="housepict" src="img/home.png" alt="SLIKA1">
                </div>
                <h3>Magna etiam</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </div>
              <div class="cells" id="second">
                <div id="secondribbon">
                  <img class="housepict" src="img/home.png" alt="SLIKA2">
                </div>
                <h3>Ipsum dolor</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </div>
              <div class="cells" id="third">
                <div id="thirdribbon">
                  <img class="housepict" src="img/home.png" alt="SLIKA3">
                </div>
                <h3>Sed feugiat</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </div>
              <div class="cells" id="fourth">
                <div id="fourthribbon">
                  <img class="housepict" src="img/home.png" alt="SLIKA4">
                </div>
                <h3>Enim phasellus</h3>
                <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
              </div>
            </div>
            <div class="callTheAction">
              <div id="callTheActionLeft">
                <div id="pictureCallTheActionLeft">
                  <img id="firstGray" src="img/druga.jpg">
                </div>
                <div>
                  <h3>Sed lorem adipiscing</h3>
                  <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
                  <button type="button" class="button_3">Learn More</button>
                </div>
              </div>
              <div id="callTheActionRight">
                <div id="pictureCallTheActionRight">
                  <img id="secondGray" src="img/treca.jpg">
                </div>
                <div>
                  <h3>Sed lorem adipiscing</h3>
                  <p>Integer volutpat ante et accumsan commophasellus sed aliquam feugiat lorem aliquet ut enim rutrum phasellus iaculis accumsan dolore magna aliquam veroeros.</p>
                  <button type="button" class="button_4">Learn More</button>
                </div>
              </div>
            </div>
          </div>
        </section>

        <footer>
          <div class="container">
            <div id="smallPictures">
              <img src="img/icons.png" alt="picture" usemap="navmap"/>
              <map name="navmap" id="map1">
                  <area alt="1" title="1" href="#" shape="rect" coords="3,5,48,45" />
                  <area alt="2" title="2" href="#" shape="rect" coords="53,6,99,46" />
                  <area alt="3" title="3" href="#" shape="rect" coords="103,7,148,47" />
                  <area alt="4" title="4" href="#" shape="rect" coords="153,8,198,48" />
                  <area alt="5" title="5" href="#" shape="rect" coords="203,10,248,48" />
                  <area alt="6" title="6" href="#" shape="rect" coords="254,6,298,48" />
              </map>
            </div>
            <p id="last">Untitled. All rights reserved. Design <a href="#">HTML5 UP</a></p>
          </div>
        </footer>

      </body>
    </html>';

  }
  else{
    echo '<p>UNAUTHORIZED ENTRY</p>';
  }
 ?>
